# Instruction
To reproduce the analysis in "The influence of socioeconomic
characteristics on active travel in US metropolitan areas and its
contribution to health inequity" (Younkin et al. 2023) submitted to
Wellcome Open Research in March 2023 run the following two commands
with GNU Make.

```bash
make data
make WOR
```

The analysis is organized as a series of R markdown scripts run from
within the Makefile with the following syntax.

```R
R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH");
Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":"));
rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir =
"~/NHTS/html", input = "./R/ScriptName.Rmd", output_file =
"./html/ScriptName.html")'
```

Note that pathnames may need to be changed.

Two files with estimates from the Global Burden of Disease
(https://ghdx.healthdata.org/gbd-2019) and the US Census
(https://data.census.gov/) are needed to generate absolute counts for the
reduction in deaths. (See Health Estimates in manuscript.)

_IHME-GBD_2019_DATA-32995d9a-1.csv_ and
_DECENNIALSF12010.P1_data_with_overlays_2022-03-03T183047.csv_ are
contained in this repository in ./data/.

# How to cite this material
Please use DOI 10.5281/zenodo.7802854 to cite this software repository.
