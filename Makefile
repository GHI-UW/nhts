WOR: WOR-2009 WOR-2017 WOR-D0 WOR-states WOR-health WOR-walk WOR-cycle WOR-pvalues-table WOR-health WOR-forests WOR-figure-data

WOR-2009: WOR-1-2009 WOR-2-2009 WOR-3-2009 WOR-4-2009 WOR-forest-1-2009 WOR-forest-2-2009 WOR-forest-3-2009
WOR-2017: WOR-1-2017 WOR-2-2017 WOR-3-2017 WOR-4-2017 WOR-forest-1-2017 WOR-forest-2-2017 WOR-forest-3-2017
WOR-forests: WOR-forest-participation WOR-forest-prevalence WOR-forest-participation-glm WOR-forest-prevalence-glm WOR-forest-intensity-glm WOR-forest-combined
WOR-walk: WOR-walk-2009 WOR-walk-2017 WOR-forest-walk WOR-forest-walk-glm
WOR-cycle: WOR-cycle-2009 WOR-cycle-2017 WOR-forest-cycle WOR-forest-cycle-glm
WOR-health: WOR-health-2009 WOR-health-2017 WOR-health-table

data:
	cd data; wget -nc https://nhts.ornl.gov/assets/2016/download/Csv.zip; unzip -o Csv.zip;
	cd data; wget -nc https://nhts.ornl.gov/2009/download/Ascii.zip; unzip -o Ascii.zip; rm -rf NHTS2009/; mv -vf Ascii/ NHTS2009;

WOR-1-2009: R/WOR-1-2009.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-1-2009.Rmd", output_file = "./html/WOR-1-2009.html")'

WOR-1-2017: R/WOR-1-2017.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-1-2017.Rmd", output_file = "./html/WOR-1-2017.html")'

WOR-2-2009: R/WOR-2-2009.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-2-2009.Rmd", output_file = "./html/WOR-2-2009.html")'

WOR-2-2017: R/WOR-2-2017.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-2-2017.Rmd", output_file = "./html/WOR-2-2017.html")'

WOR-pvalues-table: R/WOR-pvalues-table.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-pvalues-table.Rmd", output_file = "./html/WOR-pvalues-table.html")'

WOR-forest-1-2009:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-1-2009.Rmd", output_file = "./html/WOR-forest-1-2009.html")'

WOR-forest-1-2017:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-1-2017.Rmd", output_file = "./html/WOR-forest-1-2017.html")'

WOR-3-2009: R/WOR-3-2009.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-3-2009.Rmd", output_file = "./html/WOR-3-2009.html")'

WOR-4-2009: R/WOR-4-2009.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-4-2009.Rmd", output_file = "./html/WOR-4-2009.html")'

WOR-4-2017: R/WOR-4-2017.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-4-2017.Rmd", output_file = "./html/WOR-4-2017.html")'

WOR-3-2017: R/WOR-3-2017.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-3-2017.Rmd", output_file = "./html/WOR-3-2017.html")'

WOR-forest-2-2009:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-2-2009.Rmd", output_file = "./html/WOR-forest-2-2009.html")'

WOR-forest-2-2017:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-2-2017.Rmd", output_file = "./html/WOR-forest-2-2017.html")'

WOR-D0: R/WOR-D0.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-D0.Rmd", output_file = "./html/WOR-D0.html")'

WOR-forest-3-2009:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-3-2009.Rmd", output_file = "./html/WOR-forest-3-2009.html")'

WOR-forest-3-2017:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-3-2017.Rmd", output_file = "./html/WOR-forest-3-2017.html")'

WOR-states: R/WOR-states.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-states.Rmd", output_file = "./html/WOR-states.html")'

WOR-forest-participation:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-participation.Rmd", output_file = "./html/WOR-forest-participation.html")'

WOR-forest-prevalence:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-prevalence.Rmd", output_file = "./html/WOR-forest-prevalence.html")'

WOR-forest-participation-glm:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-OR-participation.Rmd", output_file = "./html/WOR-forest-OR-participation.html")'

WOR-forest-prevalence-glm:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-OR-prevalence.Rmd", output_file = "./html/WOR-forest-OR-prevalence.html")'

WOR-forest-intensity-glm:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-OR-intensity.Rmd", output_file = "./html/WOR-forest-OR-intensity.html")'

WOR-forest-combined:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-combined.Rmd", output_file = "./html/WOR-forest-combined.html")'

WOR-figure-data:
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-figure-data.Rmd", output_file = "./html/WOR-figure-data.html")'

WOR-walk-2009: R/WOR-walk-2009.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-walk-2009.Rmd", output_file = "./html/WOR-walk-2009.html")'

WOR-cycle-2009: R/WOR-cycle-2009.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-cycle-2009.Rmd", output_file = "./html/WOR-cycle-2009.html")'

WOR-walk-2017: R/WOR-walk-2017.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-walk-2017.Rmd", output_file = "./html/WOR-walk-2017.html")'

WOR-cycle-2017: R/WOR-cycle-2017.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-cycle-2017.Rmd", output_file = "./html/WOR-cycle-2017.html")'

WOR-forest-walk: R/WOR-forest-walk.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-walk.Rmd", output_file = "./html/WOR-forest-walk.html")'

WOR-forest-cycle: R/WOR-forest-cycle.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-cycle.Rmd", output_file = "./html/WOR-forest-cycle.html")'

WOR-forest-walk-glm: R/WOR-forest-OR-walk.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-OR-walk.Rmd", output_file = "./html/WOR-forest-OR-walk.html")'

WOR-forest-cycle-glm: R/WOR-forest-OR-cycle.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-forest-OR-cycle.Rmd", output_file = "./html/WOR-forest-OR-cycle.html")'

WOR-figure-data: R/WOR-figure-data.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-figure-data.Rmd", output_file = "./html/WOR-figure-data.html")'

WOR-health-2009: R/WOR-health-2009.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-health-2009.Rmd", output_file = "./html/WOR-health-2009.html")'

WOR-health-2017: R/WOR-health-2017.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-health-2017.Rmd", output_file = "./html/WOR-health-2017.html")'

WOR-health-table: R/WOR-health-table.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/WOR-health-table.Rmd", output_file = "./html/WOR-health-table.html")'


USA-2022-all: data2022 USA-2022 USA-2022-modeling USA-2022-figures

data2022:
	cd data; test -d 2022 || mkdir 2022
	cd data/2022; wget https://nhts.ornl.gov/assets/2022/download/Csv.zip
	cd data/2022; unzip Csv.zip
	cd data/2022; rm -vf Csv.zip

USA-2022: R/USA-2022.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/USA-2022.Rmd", output_file = "./html/USA-2022.html")'

USA-2022-modeling: R/USA-2022-modeling.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/USA-2022-modeling.Rmd", output_file = "./html/USA-2022-modeling.html")'

USA-2022-figures: R/USA-2022-figures.Rmd
	R -e 'library("rmarkdown"); old_path <- Sys.getenv("PATH"); Sys.setenv(PATH = paste(old_path, "/usr/local/bin", sep = ":")); rmarkdown::render(knit_root_dir = "~/NHTS/", output_dir = "~/NHTS/html", input = "./R/USA-2022-figures.Rmd", output_file = "./html/USA-2022-figures.html")'


clean: clean-data clean-figure clean-script

clean-data:
	rm -vf ./R/data/*.rds

clean-script:
	rm -rvf ./*.md

clean-figure:
	rm -rvf ./figure/

.PHONY: data
