---
title: "NHTS - Forest Plot - Intensity"
output:
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
      smooth_scroll: true
---

```{r libs, eval = TRUE, echo = FALSE, results = "show", warning = TRUE, error = TRUE, message = FALSE}
setwd("~/NHTS/")
rm(list=ls())
library("HOT")
library(ggplot2)
fig.height <- 4
set.seed(1)
```

```{r data, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
USA.full.2009 <- read.csv(file = "./R/data/intensity.regression.2009.csv")
USA.2009 <- USA.full.2009[1:31,] # excluding the variable products

USA.full.2017 <- read.csv(file = "./R/data/intensity.regression.2017.csv")
USA.2017 <- USA.full.2017[1:34,] # excluding the variable products
```

```{r refactor, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
popdensitynames <- c("pop.density50", "pop.density300", "pop.density750", "pop.density1500", "pop.density7000", "pop.density17000", "pop.density30000")
incomenames.2009 <- c("income1", "income2", "income3", "income4", "income6", "income7", "income8")
edunames <- c("education1", "education3", "education4", "education5")
racenames <- c("race2", "race3", "race4", "race5", "race6", "race97")
ncarnames <- c("ncar0","ncar1","ncar3+")

incomenames.2017 <- c("income01", "income02", "income03", "income04", "income06", "income07", "income08", "income09", "income010", "income011")


USA.2009 <- USA.2009 %>%
  mutate(varname = ifelse(X == "(Intercept)", "(Intercept)",
                          ifelse(X == "age.numeric", "Age",
                                 ifelse(X %in% ncarnames, "Cars",
                                        ifelse(X == "hispanic1", "Hisp.",
                                               ifelse(X == "sexF", "Sex",
                                                      ifelse(X %in% popdensitynames, "Population Density",
                                                             ifelse(X %in% incomenames.2009, "Household Income",
                                                                    ifelse(X %in% edunames, "Education",
                                                                           ifelse(X %in% racenames, "Race", NA))))))))))

USA.2009 <- within(USA.2009,{
  varname <- factor(varname, levels = c("Sex","Age","Race","Hisp.","Household Income","Education","Population Density","Cars", "(Intercept)"))
})

USA.2017 <- USA.2017 %>%
  mutate(varname = ifelse(X == "(Intercept)", "(Intercept)",
                          ifelse(X == "age.numeric", "Age",
                                 ifelse(X %in% ncarnames, "Cars",
                                        ifelse(X == "hispanic1", "Hisp.",
                                               ifelse(X == "sexF", "Sex",
                                                      ifelse(X %in% popdensitynames, "Population Density",
                                                             ifelse(X %in% incomenames.2017, "Household Income",
                                                                    ifelse(X %in% edunames, "Education",
                                                                           ifelse(X %in% racenames, "Race", NA))))))))))

USA.2017 <- within(USA.2017,{
  varname <- factor(varname, levels = c("Sex","Age","Race","Hisp.","Household Income","Education","Population Density","Cars", "(Intercept)"))
})

USA.2009$X <- recode(USA.2009$X, "sexF" = "Female",
                     "age.numeric" = "Age",
                     "pop.density50" = "0 to 99",
                     "pop.density300" = "100 to 499",
                     "pop.density750" = "500 to 999",
                     "pop.density1500" = "1,000 to 1,999",
                     "pop.density7000" = "4,000 to 9,999",
                     "pop.density17000" = "10,000 to 24,999",
                     "pop.density30000" = "> 25,000",
                     "income1" = "< $10,000",
                     "income2" = "$10,000 to $14,999",
                     "income3" = "$15,000 to $24,999",
                     "income4" = "$25,000 to $34,999",
                     "income6" = "$50,000 to $74,999",
                     "income7" = "$75,000 to $99,999",
                     "income8" = "> $100,000",
                     "education1" = "Less than High School",
                     "education3" = "Some College/Associates Degree",
                     "education4" = "Bachelor's Degree",
                     "education5" = "Graduate/Professional Degree",
                     "ncar0" = "0",
                     "ncar1" = "1",
                     "ncar3+" = "3+",
                     "race2" = "Black or African American",
                     "race3" = "Asian",
                     "race4" = "American Indian or Alaska Native",
                     "race5" = "Native Hawaiian or other Pacific Islander",
                     "race6" = "Multiple Responses Selected",
                     "race97" = "Other",
                     "hispanic1" = "Hispanic"
)

USA.2017$X <- recode(USA.2017$X, "sexF" = "Female",
                     "age.numeric" = "Age",
                     "pop.density50" = "0 to 99",
                     "pop.density300" = "100 to 499",
                     "pop.density750" = "500 to 999",
                     "pop.density1500" = "1,000 to 1,999",
                     "pop.density7000" = "4,000 to 9,999",
                     "pop.density17000" = "10,000 to 24,999",
                     "pop.density30000" = "> 25,000",
                     "income01" = "< $10,000",
                     "income02" = "$10,000 to $14,999",
                     "income03" = "$15,000 to $24,999",
                     "income04" = "$25,000 to $34,999",
                     "income06" = "$50,000 to $74,999",
                     "income07" = "$75,000 to $99,999",
                     "income08" = "$100,000 to $124,999",
                     "income09" = "$125,000 to $149,999",
                     "income010" = "$150,000 to $199,999",
                     "income011" = "> $200,000",
                     "education1" = "Less than High School",
                     "education3" = "Some College/Associates Degree",
                     "education4" = "Bachelor's Degree",
                     "education5" = "Graduate/Professional Degree",
                     "ncar0" = "0",
                     "ncar1" = "1",
                     "ncar3+" = "3+",
                     "race2" = "Black or African American",
                     "race3" = "Asian",
                     "race4" = "American Indian or Alaska Native",
                     "race5" = "Native Hawaiian or other Pacific Islander",
                     "race6" = "Multiple Responses Selected",
                     "race97" = "Other",
                     "hispanic1" = "Hispanic"
)

USA.2009 <- USA.2009 %>%
  filter(!(varname %in% c("(Intercept)","Age"))) %>%
  mutate(X = as.factor(X)) %>%
  mutate(varname = as.factor(varname)) %>%
  mutate(Index = c(1:length(X))) %>%
  mutate(Index = ifelse(Index == 15, 14.5, Index)) %>% # move "> $100,000" above "$100,000 to $124,999"
  mutate(Year = "2009")

USA.2017 <- USA.2017 %>%
  filter(!(varname %in% c("(Intercept)","Age"))) %>%
  mutate(X = as.factor(X)) %>%
  mutate(varname = as.factor(varname)) %>%
  mutate(Index = c(1:length(X))) %>%
  mutate(Year = "2017")
```

```{r combine_years, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
USA <- rbind(USA.2009, USA.2017)
```

```{r format, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
USA <- USA %>%
  mutate(Year = factor(Year, levels = c("2017", "2009"))) %>%
  dplyr::filter(X != "American Indian or Alaska Native") %>%
  dplyr::filter(X != "Native Hawaiian or other Pacific Islander") %>%
  mutate(Index = ifelse(varname == "Race", ifelse(X == "Asian", 23,
                                                 ifelse(X == "American Indian or Alaska Native", 24,
                                                        ifelse(X == "Black or African American", 25,
                                                               ifelse(X == "Multiple Responses Selected", 26,
                                                                      ifelse(X == "Native Hawaiian or other Pactific Islander", 27,
                                                                             ifelse(X == "Other", 28, Index)))))), Index))
```

```{r forest, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
p <- USA %>% ggplot(aes(y = reorder(X, desc(Index)), x = mean, xmin = lower, xmax = upper)) +
  geom_point(aes(fill = Year, color = Year, shape = Year), size = 2, position = position_dodge(width = 0.75)) +
  geom_errorbarh(aes(color = Year), height = 0.1, position = position_dodge(width = 0.75), show.legend = FALSE) + # error bar
  geom_vline(xintercept = 0, color = "grey50", linetype = "dashed", alpha = 0.5) + # dotted line at effect = 0 (e^0 = 1)
  facet_grid(varname ~ ., scales = "free", space = "free", labeller = labeller(varname = label_wrap_gen(width = 10))) + # subgroup faceting
  scale_fill_manual(values = c("grey50", "grey")) +
  scale_color_manual(values = c("grey50", "grey")) +
  scale_shape_manual(values = c(16,17)) +
  ggtitle("\nIntensity of Active Travel\nMultivariate Linear Regression\n") +
  xlab("\nRegression Coefficient\n") +
  ylab("") +
  guides(fill = guide_legend(reverse = TRUE), color = guide_legend(reverse = TRUE), shape = guide_legend(reverse = TRUE)) +
  theme(plot.background = element_rect(fill = "white")) + # fixes blank background when saving image
  theme(text = element_text(family = "Times", size = 12, color = "black")) + # overall text formatting
  theme(plot.title = element_text(hjust = 0.5)) + # center plot title
  theme(panel.spacing = unit(0.5, "lines"), strip.text.y = element_text(angle = 0, vjust = 0.5, hjust = 0.5, size = 12)) + # adjust facet panel spacing, text formatting
  coord_cartesian(clip = "off") + # prevent clipping of geom_points at facet panel margins
  # scale_x_log10(breaks = breaks <- 2^(-3:3), minor_breaks = breaks) +
  theme_bw()

p %>% ggsave(filename = "./figure/WOR_forest_intensity_glm.pdf", width = 6.5, height = 9, units = "in")
p %>% ggsave(filename = "./figure/fig3.jpg", width = 6.5, height = 9, units = "in", dpi = 300)


# saving ggplot for combining with caption, raw data for journal
# saveRDS(p, file = "./R/data/WOR_int_OR_ggplot.rds")
saveRDS(USA, file = "./R/data/WOR_int_OR_raw.rds")
```

# R Session Info

```{r sessionInfo, eval = TRUE, echo = FALSE, results = "show", warning = FALSE, error = FALSE, message = FALSE, fig.height = fig.height}
date()
sysInfo <- Sys.info()
sysInfo[names(sysInfo) != "version"]
sessionInfo()
```
