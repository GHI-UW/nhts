---
title: "HOT U.S. Analysis"
output:
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
      smooth_scroll: true
---

# Walk - USA 2017 (All)
## Analysis
```{r libs, eval = TRUE, echo = FALSE, results = "show", warning = TRUE, error = TRUE, message = FALSE}
rm(list=ls())
library("HOT")
fig.height <- 4
set.seed(1)
```

### Trip Table
```{r trippub, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
trippub.raw <- read.csv(file = "./data/trippub.csv", stringsAsFactors = FALSE)

trippub <- within(trippub.raw,{
  houseID = as.character(HOUSEID)
  subjectID = as.character(PERSONID)
  duration = ifelse(as.numeric(TRVLCMIN) < 0, as.numeric(NA), as.numeric(TRVLCMIN))
  #dist = ifelse(as.numeric(TRPMILES) < 0, as.numeric(NA), as.numeric(TRPMILES))
  mode = TRPTRANS
  weight.trip = WTTRDFIN # trip weight
})

trippub <- trippub %>% mutate(mode = factor(ifelse(mode %in% c(-9,-8,-7), NA, ifelse(mode == 1, "walk", ifelse(mode == 2, "cycle", "other"))), levels = c("walk", "cycle", "other")))

(n.trip.0 <- nrow(trippub))

(n.trip.NA <- sum(is.na(trippub$mode)))

(n.trip.exercise <- sum(trippub$WHYTO == "16"))

trippub <- trippub %>% filter(!is.na(mode) & WHYTO != 51) %>% select(houseID, subjectID, duration, mode)

(n.trip <- nrow(trippub))

n.trip.NA/n.trip.0
n.trip.exercise/n.trip.0
```
#### Filter Criteria
No missing values for mode and no trip purpose of "Go to gym/exercise/play sports."

### Person Table
```{r perpub, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
perpub.raw <- read.csv(file = "./data/perpub.csv", stringsAsFactors = FALSE)
perpub <- within(perpub.raw,{
  houseID = as.character(HOUSEID)
  subjectID = as.character(PERSONID)
  age = ifelse(R_AGE %in% c(-8,-7), NA, R_AGE)
  age = factor(ifelse(!is.na(age), ifelse(age <= 18, "child", ifelse(age <= 65, "adult", "senior")), NA), levels = c("child","adult","senior"))
  sex = factor(ifelse(R_SEX=="1", "M", ifelse(R_SEX=="2", "F", NA)), levels = c("M","F"))
  weight.person = WTPERFIN
  nwalk = ifelse(NWALKTRP < 0, NA, NWALKTRP)
  nbike = ifelse(NBIKETRP < 0, NA, NBIKETRP)
})


table(perpub$age, useNA = "always")
(n <- as.numeric(sum(table(perpub$age, useNA = "always"))))

c(table(perpub$age, useNA = "always")["adult"])/n

perpub <- perpub %>% filter(age == "adult")

n.adult <- nrow(perpub)

perpub <- perpub %>% filter(!is.na(nwalk) & !is.na(nbike)) %>% select(houseID, subjectID, sex, age)

n.adult2 <- nrow(perpub)

c(n,n.adult,n.adult2)

table(perpub$age, useNA = "always")["adult"]
```

#### Filter Criteria
Adults only and no missing data for NWALKTRP and NBIKETRP.

### House Tables

```{r hhpub, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
hhpub.raw <- read.csv(file = "./data/hhpub.csv", stringsAsFactors = FALSE)
length(hhpub.raw$HOUSEID)
length(unique(hhpub.raw$HOUSEID))
```

```{r metroFile, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
metroFile <- read.csv(file = "./data/US-metro-areas.csv", header = FALSE, col.names = c("MetroName","Region","Division", "MetroCode","Megapolis"))
```

```{r hhpub2, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}

hhpub <- within(hhpub.raw,{
  HH_CBSA <- ifelse(HH_CBSA == 31100, 31080, HH_CBSA)
})

CBSA.vec <- metroFile$Region
names(CBSA.vec) <- metroFile$MetroCode

hhpub.region <- within(hhpub,{
  houseID = as.character(HOUSEID)
  location = factor(CENSUS_R, levels = 1:4, labels = c("NE","MW","S","W"))
  year = "2017"
})

hhpub.region <- hhpub.region %>% select(houseID, location, year)

```
#### Filter Criteria
None.

### Participation
```{r participationNHTS, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
active.df <- perpub.raw %>% select(HOUSEID, NWALKTRP, NBIKETRP) %>% mutate(NWALKTRP = ifelse(NWALKTRP < 0, NA, NWALKTRP), NBIKETRP = ifelse(NBIKETRP < 0, NA, NBIKETRP)) %>% mutate(active = NWALKTRP > 0 | NBIKETRP > 0 ) %>% mutate(houseID = as.character(HOUSEID)) %>% select(houseID, active)

participation.region.df <- inner_join(active.df, hhpub.region, by = "houseID") %>% group_by(location) %>% summarize(participation = sum(active, na.rm = TRUE)/sum(!is.na(active))) %>% select(location, participation)
saveRDS(participation.region.df, file = "./R/data/participation.region.walk.2017.rds")
```

### Travel Survey Objects

```{r ts, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
USA.region <- createTravelSurvey(person=perpub, house=hhpub.region, trip=trippub, location = participation.region.df)
summary(getTravelActivity(USA.region))
USA.region <- removeOutliers(USA.region, lower = 0)
saveRDS(USA.region, file = "./R/data/USA.2017.region.walk.ts.rds")
```


## Sample Size

```{r samplesize, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
getSampleSize(USA.region) %>% select(location,person) %>% as.data.frame()
```

# Demographic Data Frame

```{r D, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
USA <- readRDS(file = "./R/data/USA.2017.region.walk.ts.rds")

perpub.raw <- read.csv(file = "./data/perpub.csv", stringsAsFactors = FALSE)

D0 <- perpub.raw %>% select(houseID = HOUSEID, subjectID = PERSONID, date = TDAYDATE, ncar = HHVEHCNT, race = R_RACE, hispanic = R_HISP, income = HHFAMINC, education = EDUC, pop.density = HBPPOPDN, house.density = HBRESDN, physical.activity = VPACT, US.born = BORNINUS, age.numeric = R_AGE, nwalk = NWALKTRP, ncycle = NBIKETRP, weight.person = WTPERFIN)

D0 <- within( D0, {
  houseID <- as.character(houseID)
  subjectID = as.character(subjectID)
  #season <- factor(ifelse(date %in% c("201604","201605","201606","201607","201608","201609", "201610","201704"), "in","out"), levels = c("in","out"))
  season <- factor(ifelse(date %in% c("201603", "201604", "201605","201703","201704"), "spring", ifelse( date %in% c("201606","201607","201608"), "summer", ifelse( date %in% c("201609", "201610","201611"), "fall", ifelse( date %in% c("201612","201701","201702"), "winter", NA)))), levels = c("spring","summer","fall","winter"))
  date <- factor(date)
  ncar <- factor(ifelse(ncar==0,"0",ifelse(ncar==1,"1",ifelse(ncar ==2, "2", ifelse(ncar >=3, "3+", NA)))))
  race <- factor(ifelse(race %in% c(-1,-7,-8,-9), NA, ifelse(race == 7, 97, race)))
  race <- factor(ifelse(is.na(race), NA, ifelse(!(race %in% c(1,2)), 3, race)))
  hispanic <- ifelse(hispanic == 2, 0, hispanic)
  hispanic <- factor(ifelse(hispanic %in% c(-1,-7,-8,-9), NA, hispanic))
  income <- ifelse(income %in% c(-7,-8,-9), NA, income)
  income <- factor(ifelse(income %in% c(1,2), 1, ifelse(income == 3, 2, ifelse(income %in% c(4,5), 3, ifelse(income %in% c(6,7), 4, ifelse(income %in% c(8:10), 5, ifelse(income %in% c(11:15), 6, ifelse(income %in% c(16,17), 7, 8))))))))
  education <- factor(ifelse(education %in% c(-1,-7,-8,-9), NA, education))
  pop.density  <- factor(ifelse(pop.density == -9, NA, pop.density))
  house.density  <- factor(ifelse(house.density == -9, NA, house.density))
  US.born  <- factor(ifelse(US.born %in% c(-1,-7,-8,-9), NA, US.born))
  age.numeric = ifelse(age.numeric %in% c(-8,-7), NA, age.numeric - 40)
  nwalk <- as.numeric(ifelse(nwalk %in% c(-7,-8,-9), NA, nwalk))
  ncycle <- as.numeric(ifelse(ncycle %in% c(-7,-8,-9), NA, ncycle))
})

D0 <- within(D0,{
  income <- relevel(income, ref = '5') # 35,000 - 49,999
  education <- relevel(education, ref = '2') # High School graduate or GED
  pop.density <- relevel(pop.density, ref = '3000') # Pop density of Madison
  ncar <- relevel(ncar, ref = '2')
  season <- relevel(season, ref = 'fall')
})

D0 <- inner_join(USA@person, D0, by = c("houseID", "subjectID"))
saveRDS(D0, file = "./R/data/D0-2017.walk.rds")
```






```{r libs2, eval = TRUE, echo = FALSE, results = "show", warning = TRUE, error = TRUE, message = FALSE}
rm(list=ls())
fig.height <- 4
set.seed(1)
```

```{r setup, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
USA <- readRDS(file = "./R/data/USA.2017.region.walk.ts.rds")
D0 <- readRDS(file = "./R/data/D0-2017.walk.rds")

# TA.df <- getTravelActivity(USA, activeTravelers=FALSE, weekly = FALSE)
TA.df <- getDurationByMode(USA) %>% select(location, houseID, subjectID, TA = walk)


D <- inner_join(TA.df, D0, by = c( "houseID", "subjectID"))
D <- within(D,{
  location <- relevel(location, ref = 'S')
})
```

```{r prevalence_D, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
(D0 <- D %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se))

D1 <- D %>% group_by(sex) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("sex")) %>% select(factor, group = sex, prevalence = p, lower, upper)

D2 <- D %>% group_by(race) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("race")) %>% select(factor, group = race, prevalence = p, lower, upper)

D3 <- D %>% group_by(hispanic) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("hispanic")) %>% select(factor, group = hispanic, prevalence = p, lower, upper)

D4 <- D %>% group_by(education) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("education")) %>% select(factor, group = education, prevalence = p, lower, upper)

D5 <- D %>% group_by(income) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("income")) %>% select(factor, group = income, prevalence = p, lower, upper)

D6 <- D %>% group_by(ncar) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("ncar")) %>% select(factor, group = ncar, prevalence = p, lower, upper)

D7 <- D %>% group_by(pop.density) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("pop.density")) %>% select(factor, group = pop.density, prevalence = p, lower, upper)

D.all <- rbind(D1,D2,D3,D4,D5,D6,D7)
# levels(D.all$factor) <- c("sex", "race", "hispanic", "income", "education")
```

```{r prevalence_recode, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
D.fig <- D.all %>% mutate(combined = paste0(factor, group)) %>% mutate(factor = factor(factor, levels = c("sex", "race", "hispanic", "income", "education", "ncar", "pop.density")))

D.fig$combined <- recode(D.fig$combined,
                         "sexM" = "Male",
                         "sexF" = "Female",
                         "race1" = "White",
                         "race2" = "Black or African American",
                         "race3" = "Other",
                         # "race4" = "American Indian or Alaska Native",
                         # "race5" = "Native Hawaiian or other Pacific Islander",
                         # "race6" = "Multiple Responses Selected",
                         # "race97" = "Other",
                         "hispanic0" = "Not Hispanic",
                         "hispanic1" = "Hispanic",
                         "education1" = "Less than High School",
                         "education2" = "High School Diploma",
                         "education3" = "Some College/Associates Degree",
                         "education4" = "Bachelor's Degree",
                         "education5" = "Graduate/Professional Degree",
                         "income1" = "< $10,000",
                         "income2" = "$10,000 to $14,999",
                         "income3" = "$15,000 to $24,999",
                         "income4" = "$25,000 to $34,999",
                         "income5" = "$35,000 to $49,999",
                         "income6" = "$50,000 to $74,999",
                         "income7" = "$75,000 to $99,999",
                         "income8" = "$100,000 or more",
                         "ncar0" = "0",
                         "ncar1" = "1",
                         "ncar2" = "2",
                         "ncar3+" = "3+",
                         "pop.density50" = "0 to 99",
                         "pop.density300" = "100 to 499",
                         "pop.density750" = "500 to 999",
                         "pop.density1500" = "1,000 to 1,999",
                         "pop.density3000" = "2,000 to 3,999",
                         "pop.density7000" = "4,000 to 9,999",
                         "pop.density17000" = "10,000 to 24,999",
                         "pop.density30000" = "> 25,000"
)

D.fig$factor <- recode(D.fig$factor, 
                       "sex" = "Sex", 
                       "race" = "Race", 
                       "hispanic" = "Hispanic", 
                       "education" = "Education", 
                       "income" = "Household Income", 
                       "ncar" = "Cars", 
                       "pop.density" = "Population Density")

D.fig <- D.fig %>%
  mutate(combined = as.factor(combined)) %>%
  mutate(Index = c(1:length(combined))) %>%
  mutate(Index = ifelse(combined == "$35,000 to $49,999", 21.5, ifelse(combined == "High School Diploma", 12.5, ifelse(combined == "2", 26.5, ifelse(combined == "2,000 to 3,999", 32.5, Index))))) %>%
  relocate(combined, .after = factor) %>%
  dplyr::filter(!(combined %in% c("sexNA", "raceNA", "hispanicNA", "educationNA", "incomeNA", "pop.densityNA"))) %>%
  select(-group)

saveRDS(D.fig, file = "./R/data/prevalence.fig.walk.2017.rds")
```

```{r prevalence_forest, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
p <- D.fig %>%
  ggplot(aes(y = reorder(combined, desc(Index)), x = prevalence, xmin = lower, xmax = upper)) +
  geom_point(aes(), color = "black", pch = 21) +
  geom_errorbarh(height = 0.1) +
  geom_vline(xintercept = D0$p, color = "black", linetype = "dashed", alpha = 0.5) +
  facet_grid(factor ~ ., scales = "free", space = "free", labeller = labeller(varname = label_wrap_gen(width = 10))) +
  ggtitle("\nPrevalence of Walking (2017)\n") +
  xlab("\nPrevalence\n") +
  ylab("") +
  theme(text = element_text(family = "Times", size = 12, color = "black")) + # overall text formatting
  theme(plot.title = element_text(hjust = 0.5)) + # center plot title
  theme(panel.spacing = unit(0.5, "lines"), strip.text.y = element_text(angle = 0, vjust = 0.5, hjust = 0.5, size = 12)) + # adjust facet panel spacing, text formatting
  coord_cartesian(clip = "off") + # prevent clipping of geom_points at facet panel margins
  theme_bw()

p %>% ggsave(filename = "./figure/WOR_forest_prevalence-2017.walk.png", width = 6.5, height = 9, units = "in")
```

# Regression Model, Prevalence, 2017

```{r allall1, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
glm.obj <- D %>% mutate(I_TA = TA > 0) %>% glm(I_TA ~ sex + age.numeric + pop.density + income + education + ncar + race*hispanic + location + location:season, family = "binomial", data = .)
summary(glm.obj)
anova(glm.obj, test = "Chisq")

saveRDS(glm.obj, file = "./R/data/glm.prevalence.walk.2017.rds")

foo <- as.data.frame(summary(glm.obj)$coeff)
names(foo) <- c("estimate","sd","z","p")
foo <- foo %>% select(estimate, sd, p) %>% mutate( lower = estimate - 1.96*sd, upper = estimate + 1.96*sd) %>% mutate( OR = exp(estimate), OR.lower = exp(lower), OR.upper = exp(upper)) %>% select(estimate, OR, p, OR.lower, OR.upper)
write.csv(foo, file = "./R/data/prevalence.regression.walk.2017.csv", quote = FALSE)
```

# Regression Model, Prevalence, 2017 (Paul)

```{r allall, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
glm.obj <- D %>% mutate(I_TA = TA > 0) %>% glm(I_TA ~ sex + age.numeric + education + race*hispanic + location + location, family = "binomial", data = .)
summary(glm.obj)
anova(glm.obj, test = "Chisq")

saveRDS(glm.obj, file = "./R/data/glm.prevalence.walk.2017.rds")

foo <- as.data.frame(summary(glm.obj)$coeff)
names(foo) <- c("estimate","sd","z","p")
foo <- foo %>% select(estimate, sd, p) %>% mutate( lower = estimate - 1.96*sd, upper = estimate + 1.96*sd) %>% mutate( OR = exp(estimate), OR.lower = exp(lower), OR.upper = exp(upper)) %>% select(estimate, OR, p, OR.lower, OR.upper)
write.csv(foo, file = "./R/data/prevalence.regression.walk.2017.csv", quote = FALSE)
```














# R Session Info

```{r sessionInfo, eval = TRUE, echo = FALSE, results = "show", warning = FALSE, error = FALSE, message = FALSE, fig.height = fig.height}
date()
sysInfo <- Sys.info()
sysInfo[names(sysInfo) != "version"]
sessionInfo()
```
