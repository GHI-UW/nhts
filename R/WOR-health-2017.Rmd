---
title: "Health Estimates"
output:
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
      smooth_scroll: true
---


```{r libs, eval = TRUE, echo = FALSE, results = "show", warning = TRUE, error = TRUE, message = FALSE}
rm(list=ls())
library("HOT")
fig.height <- 4
set.seed(1)
```

```{r summary2, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
prevalence.2017 <- read.csv(file = "./R/data/prevalence.regression.2017.csv")
beta0 <- prevalence.2017[1,"estimate"]
beta <- prevalence.2017[-1,"estimate"]
p0 <- exp(beta0)/(1+exp(beta0))
p.vec <- (exp(beta0 + beta)/(1+exp(beta0 + beta)))

participation.2017 <- read.csv(file = "./R/data/participation.regression.2017.csv")
beta0 <- participation.2017[1,"estimate"]
beta <- participation.2017[-1,"estimate"]
pi0 <- exp(beta0)/(1+exp(beta0))
pi.vec <- (exp(beta0 + beta)/(1+exp(beta0 + beta)))

f0 <- p0/pi0
f.vec <- p.vec/pi.vec

intensity.2017 <- read.csv(file = "./R/data/intensity.regression.2017.csv")
beta0 <- intensity.2017[1,"mean"]
beta <- intensity.2017[-1,"mean"]
mu0 <- beta0
mu.vec <- (beta0 + beta)

glm.mu.2017 <- readRDS(file = "./R/data/glm.intensity.2017.rds")
sigma0 <- sigma(glm.mu.2017)

mu.transform <- function(mu, sigma){
  exp(mu + sigma^2/2)
}

intensity0 <- 7*f0*mu.transform(mu0,sigma0)
intensity.vec <- 7*f.vec*mu.transform(mu.vec,sigma0)

sigma.transform <- function(mu, sigma){
  sqrt((exp(sigma^2)-1)*exp(2*mu+sigma^2))
}

sigma0prime <- 7*f0*sigma.transform(mu0,sigma0)
sigmaprime.vec <- 7*f.vec*sigma.transform(mu.vec,sigma0)


HOT.parameters.df <- data.frame(variable = participation.2017$X, participation = c(pi0, pi.vec), intensity = c(intensity0, intensity.vec), sigma = c(sigma0prime, sigmaprime.vec), frequency = c(f0,f.vec)) %>% mutate(TA = participation*intensity, delta = TA - TA[1], pctChange = 100*delta/TA[1])


HOT.parameters.df #%>% head(10)

HOT.parameters.df[1:34,] %>% select(variable, delta, pctChange) %>% arrange(-delta)

HOT.parameters.df[35:60,] %>% select(variable, delta, pctChange) %>% arrange(-delta)

HOT.parameters.df[61:66,] %>% select(variable, delta, pctChange) %>% arrange(-delta)

HOT.parameters.df[67:147,] %>% select(variable, delta, pctChange) %>% arrange(-delta)
```

```{r cra2, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
n <- 1e6; LA.baseline  <- HOT.LA(n = n); n <- length(LA.baseline); nq <- 1e4;


TA.baseline <- HOT.TA(participation = HOT.parameters.df[1,"participation"],  intensity = HOT.parameters.df[1,"intensity"], sd = HOT.parameters.df[1,"sigma"], n = n)

q.baseline <- quantile(LA.baseline, probs = (1:nq)/nq)

rho.vec <- c()

for(i in 2:nrow(HOT.parameters.df)){

    TA.scenario <- HOT.TA(participation = HOT.parameters.df[i,"participation"],  intensity = HOT.parameters.df[i,"intensity"], sd = HOT.parameters.df[i,"sigma"], n = n)

    deltaTA <- TA.scenario -  TA.baseline

    LA.scenario <- ifelse(LA.baseline + deltaTA < 0,  0, LA.baseline + deltaTA)

    q.scenario <- quantile(LA.scenario, probs = (1:nq)/nq)

    rho <- sum(ERF.acm.arem.fun(q.scenario))/sum(ERF.acm.arem.fun(q.baseline)) - 1
    rho.vec <- c(rho.vec, rho)

}

gbd.data <- read.csv(file = "./data/IHME-GBD_2019_DATA-32995d9a-1.csv", header = TRUE)
metro.pop.data <- read.csv(file = "./data/DECENNIALSF12010.P1_data_with_overlays_2022-03-03T183047.csv", skip = 1)
n <- sum(as.numeric(metro.pop.data$Total[-1]))
n.tot <- as.numeric(strsplit(metro.pop.data$Total[1], split = "\\(")[[1]][1])
p.metro <- n/n.tot # proportion of US in major metropolitan areas in 2010
n.deaths <- gbd.data %>% group_by(year) %>% summarize(deaths = p.metro*sum(val)) %>% filter( year == "2017") %>% select(deaths) %>% as.numeric()

n.deaths

HOT.parameters.df <- HOT.parameters.df  %>% mutate(rho  = c(NA,100*rho.vec)) %>% mutate(deaths = n.deaths*rho/100)

HOT.parameters.df[1:34,] %>% select(variable, delta, pctChange, rho, deaths) %>% arrange(-rho)

HOT.parameters.df[35:60,] %>% select(variable, delta, pctChange, rho, deaths) %>% arrange(-rho)

HOT.parameters.df[61:66,] %>% select(variable, delta, pctChange, rho, deaths) %>% arrange(-rho)

HOT.parameters.df[67:147,] %>% select(variable, delta, pctChange, rho, deaths) %>% arrange(-rho)

saveRDS(HOT.parameters.df, file = "./R/data/HOT.parameters.2017.rds")
write.csv(HOT.parameters.df, file = "./R/data/HOT.parameters.2017.csv", quote = FALSE, row.names = FALSE)
```

### Data Sources

* http://ghdx.healthdata.org/gbd-results-tool
* http://ghdx.healthdata.org/gbd-results-tool?params=gbd-api-2019-permalink/9046c1b285fec83814c427bdc397cdb7
* https://data.census.gov/cedsci/table?q=population%20by%20urban&g=0100000US_01000C7US_01000C8US_01000C9US&y=2010&tid=DECENNIALSF12010.P1&tp=true

# R Session Info

```{r sessionInfo, eval = TRUE, echo = FALSE, results = "show", warning = FALSE, error = FALSE, message = FALSE, fig.height = fig.height}
date()
sysInfo <- Sys.info()
sysInfo[names(sysInfo) != "version"]
sessionInfo()
```
