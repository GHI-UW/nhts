---
title: "NHTS - Forest Plot - Prevalence - Cycle"
output:
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
      smooth_scroll: true
---

```{r libs, eval = TRUE, echo = FALSE, results = "show", warning = TRUE, error = TRUE, message = FALSE}
setwd("~/NHTS/")
rm(list=ls())
library("HOT")
library(ggplot2)
fig.height <- 4
set.seed(1)
```

```{r data, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
D.fig.2009 <- readRDS(file = "./R/data/prevalence.fig.cycle.2009.rds")
D.fig.2017 <- readRDS(file = "./R/data/prevalence.fig.cycle.2017.rds")

# add year variable, clean up income ordering
D.fig.2009 <- D.fig.2009 %>% mutate(Year = "2009")
D.fig.2017 <- D.fig.2017 %>% mutate(Year = "2017")

D.fig <- rbind(D.fig.2009, D.fig.2017)
```

```{r intercept, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
D0.2009 <- readRDS(file = "./R/data/D0-2009.cycle.rds")
USA.2009 <- readRDS(file = "./R/data/USA.2009.region.cycle.ts.rds")
D0.2017 <- readRDS(file = "./R/data/D0-2017.cycle.rds")
USA.2017 <- readRDS(file = "./R/data/USA.2017.region.cycle.ts.rds")

TA.2009.df <- getDurationByMode(USA.2009) %>% select(location, houseID, subjectID, TA = cycle)
D.2009 <- inner_join(TA.2009.df, D0.2009, by = c( "houseID", "subjectID"))
D.2009 <- within(D.2009,{ location <- relevel(location, ref = 'S') })
D0.2009 <- D.2009 %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se)

TA.2017.df <- getDurationByMode(USA.2017) %>% select(location, houseID, subjectID, TA = cycle)
D.2017 <- inner_join(TA.2017.df, D0.2017, by = c( "houseID", "subjectID"))
D.2017 <- within(D.2017,{ location <- relevel(location, ref = 'S') })
D0.2017 <- D.2017 %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se)
```

```{r format, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
D.fig <- D.fig %>% 
  mutate(Year = factor(Year, levels = c("2017", "2009"))) %>%
  dplyr::filter(combined != "American Indian or Alaska Native") %>% 
  dplyr::filter(combined != "Native Hawaiian or other Pacific Islander") %>%
  mutate(Index = ifelse(factor == "Race", ifelse(combined == "Black or African American", 3, ifelse(combined == "Other", 4, ifelse(combined == "White", 5, Index))), Index)) %>%
  mutate(Index = ifelse(factor == "Hispanic", ifelse(combined == "Not Hispanic", 8, ifelse(combined == "Hispanic", 7, Index)), Index)) %>%
  mutate(Index = ifelse(factor == "Sex", ifelse(combined == "Male", 2, 1), Index))
```

```{r plot, eval = TRUE, echo = TRUE, results = "show", warning = TRUE, error = TRUE, message = TRUE}
p <- D.fig %>% 
  ggplot(aes(y = reorder(combined, desc(Index)), x = prevalence, xmin = lower, xmax = upper)) + 
  geom_point(aes(fill = Year, color = Year), pch = 21, position = position_dodge(width = 0.75)) +   
  geom_errorbarh(aes(color = Year), height = 0.1, position = position_dodge(width = 0.75)) +   
  geom_vline(xintercept = D0.2009$p, color = "#00BFC4", linetype = "dashed", alpha = 0.5) + 
  geom_vline(xintercept = D0.2017$p, color = "#F8766D", linetype = "dashed", alpha = 0.5) + 
  facet_grid(factor ~ ., scales = "free", space = "free", labeller = labeller(varname = label_wrap_gen(width = 10))) + 
  ggtitle("\nPrevalence of Cycling\n") +
  xlab("\nPrevalence\n") +
  ylab("") +
  guides(fill = guide_legend(reverse = TRUE), color = guide_legend(reverse = TRUE)) +
  theme(text = element_text(family = "Times", size = 12, color = "black")) + # overall text formatting
  theme(plot.title = element_text(hjust = 0.5)) + # center plot title
  theme(panel.spacing = unit(0.5, "lines"), strip.text.y = element_text(angle = 0, vjust = 0.5, hjust = 0.5, size = 12)) + # adjust facet panel spacing, text formatting
  coord_cartesian(clip = "off") + # prevent clipping of geom_points at facet panel margins
  theme_bw()

p %>% ggsave(filename = "./figure/WOR_forest_prevalence_cycle.png", width = 6.5, height = 9, units = "in")

# second ggplot created and saved for combining with participation forest plot
p2 <- D.fig %>% 
  ggplot(aes(y = reorder(combined, desc(Index)), x = prevalence, xmin = lower, xmax = upper)) + 
  geom_point(aes(fill = Year, color = Year, shape = Year), size = 2, position = position_dodge(width = 0.75)) +   
  geom_errorbarh(aes(color = Year), height = 0.1, position = position_dodge(width = 0.75)) +   
  geom_vline(xintercept = D0.2009$p, color = "grey", linetype = "dashed", alpha = 0.5) + 
  geom_vline(xintercept = D0.2017$p, color = "grey50", linetype = "dashed", alpha = 0.5) + 
  facet_grid(factor ~ ., scales = "free", space = "free", labeller = labeller(varname = label_wrap_gen(width = 10)), switch = "y") +
  scale_fill_manual(values = c("grey50", "grey")) +
  scale_color_manual(values = c("grey50", "grey")) +
  scale_shape_manual(values = c(16,17)) +
  ggtitle("\nB: Prevalence of Cycling\n") +
  xlab("\nPrevalence\n") +
  ylab("") +
  guides(fill = guide_legend(reverse = TRUE), color = guide_legend(reverse = TRUE), shape = guide_legend(reverse = TRUE)) +
  theme(text = element_text(family = "Times", size = 12, color = "black")) + # overall text formatting
  theme(plot.title = element_text(hjust = 0.5)) + # center plot title
  theme(panel.spacing = unit(0.5, "lines"), strip.text.y = element_text(angle = 0, vjust = 0.5, hjust = 0.5, size = 12)) + # adjust facet panel spacing, text formatting
  coord_cartesian(clip = "off") + # prevent clipping of geom_points at facet panel margins
  scale_y_discrete(position = "right") +
  theme_bw() +
  theme(legend.position = "none")

# saving ggplot for combining with walk forest plot, raw data for journal
saveRDS(p2, file = "./R/data/WOR_cycle_ggplot.rds") ## ggplot

overall <- data.frame(factor = factor("Overall"), combined = factor("Overall"), prevalence = c(D0.2009$p, D0.2017$p), lower = c(D0.2009$lower, D0.2017$lower), upper = c(D0.2009$upper, D0.2017$upper), Index = NA, Year = c(2009, 2017))
D.fig2 <- rbind(overall, D.fig)

saveRDS(D.fig2, file = "./R/data/WOR_cycle_raw.rds") # data
```