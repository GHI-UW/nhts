---
title: "NHTS 2022 - UW HOT"
output:
  html_document:
    toc: true
    toc_depth: 6
    toc_float:
      collapsed: false
      smooth_scroll: true
---

# USA - NHTS 2022 - MODELING

Effort by the UW Health-Oriented Transportation (HOT) team to examine the latest release of the US-based NHTS dataset from 2022 (released November 2023).

Requires R packages *HOT* and *survey*.

## Load Data

```{r libs, eval = TRUE, echo = FALSE, results = "hide", warning = TRUE, error = TRUE, message = FALSE}
rm(list=ls())
library("HOT")
library("survey")

knitr::opts_chunk$set(eval = TRUE, echo = FALSE, results = "hide", warning = TRUE, error = TRUE, message = FALSE)
```

Load NHTS 2022 travelsurvey object and *D0* table. Run *getTA()* and merge with *D0*.

```{r setup, echo = TRUE, results = "show", message = TRUE}
USA <- readRDS(file = "./R/data/USA.2022.ts.rds")
D0 <- readRDS(file = "./R/data/D0-2022.rds")
TA.df <- getTravelActivity(USA, activeTravelers = FALSE, weekly = FALSE)
D <- inner_join(TA.df, D0, by = c("houseID", "subjectID"))
```

## Modeling

### Chi-squared

```{r prevchisq, echo = TRUE, results = "show", message = TRUE}
D <- D %>% mutate(p = TA > 0, pi = nwalk + ncycle > 0) %>% group_by(houseID) %>% mutate(nhouse = n()) %>% ungroup()
D <- D %>% mutate(nhouse = factor(ifelse(nhouse >= 4, "4+", as.character(nhouse))))
svy.dat <- svydesign(ids = ~1, data = D, weights = D$weight.person)

(sex.chisq.prev <- svychisq(~sex+p, svy.dat, statistic = "Chisq"))
(race.chisq.prev <- svychisq(~race+p, svy.dat, statistic = "Chisq"))
(hispanic.chisq.prev <- svychisq(~hispanic+p, svy.dat, statistic = "Chisq"))
(education.chisq.prev <- svychisq(~education+p, svy.dat, statistic = "Chisq"))
(income.chisq.prev <- svychisq(~income+p, svy.dat, statistic = "Chisq"))
(ncar.chisq.prev <- svychisq(~ncar+p, svy.dat, statistic = "Chisq"))
(nhouse.chisq.prev <- svychisq(~nhouse+p, svy.dat, statistic = "Chisq"))
prev.p <- data.frame(variable = c("sex", "race", "hispanic", "education", "income", "ncar", "nhouse"), prevp2022 = c(sex.chisq.prev$p.value, race.chisq.prev$p.value, hispanic.chisq.prev$p.value, education.chisq.prev$p.value, income.chisq.prev$p.value, ncar.chisq.prev$p.value, nhouse.chisq.prev$p.value))
```
```{r partchisq, echo = TRUE, results = "show", message = TRUE}
(sex.chisq.part <- svychisq(~sex+pi, svy.dat, statistic = "Chisq"))
(race.chisq.part <- svychisq(~race+pi, svy.dat, statistic = "Chisq"))
(hispanic.chisq.part <- svychisq(~hispanic+pi, svy.dat, statistic = "Chisq"))
(education.chisq.part <- svychisq(~education+pi, svy.dat, statistic = "Chisq"))
(income.chisq.part <- svychisq(~income+pi, svy.dat, statistic = "Chisq"))
(ncar.chisq.part <- svychisq(~ncar+pi, svy.dat, statistic = "Chisq"))
(nhouse.chisq.part <- svychisq(~nhouse+pi, svy.dat, statistic = "Chisq"))
part.p <- data.frame(variable = c("sex", "race", "hispanic", "education", "income", "ncar", "nhouse"), partp2022 = c(sex.chisq.part$p.value, race.chisq.part$p.value, hispanic.chisq.part$p.value, education.chisq.part$p.value, income.chisq.part$p.value, ncar.chisq.part$p.value, nhouse.chisq.part$p.value))

pvalues.2022.df <- full_join(prev.p, part.p, by = "variable")
saveRDS(pvalues.2022.df, file = "./R/data/pvalues.2022.rds")

```

### Prevalence

#### Subgroup analysis

Calculate subgroups' weighted prevalence with 95% confidence intervals. 

```{r prevalence_D, echo = TRUE, results = "show", message = TRUE}
(D0 <- D %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se))

D1 <- D %>% group_by(sex) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("sex")) %>% select(factor, group = sex, prevalence = p, lower, upper)

D2 <- D %>% group_by(race) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("race")) %>% select(factor, group = race, prevalence = p, lower, upper)

D3 <- D %>% group_by(hispanic) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("hispanic")) %>% select(factor, group = hispanic, prevalence = p, lower, upper)

D4 <- D %>% group_by(education) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("education")) %>% select(factor, group = education, prevalence = p, lower, upper)

D5 <- D %>% group_by(income) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("income")) %>% select(factor, group = income, prevalence = p, lower, upper)

D6 <- D %>% group_by(ncar) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("ncar")) %>% select(factor, group = ncar, prevalence = p, lower, upper)

D7 <- D %>% group_by(nhouse) %>% summarise(p = weighted.mean(TA > 0, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("nhouse")) %>% select(factor, group = nhouse, prevalence = p, lower, upper)

D.all <- rbind(D1,D2,D3,D4,D5,D6,D7)
```

Recode all values for figure(s).

```{r prevalence_recode, echo = TRUE, results = "show", message = TRUE}
D.fig <- D.all %>% mutate(combined = paste0(factor, group)) %>% mutate(factor = factor(factor, levels = c("sex", "race", "hispanic", "income", "education", "ncar", "nhouse")))

D.fig$combined <- recode(D.fig$combined,
                         "sexM" = "Male",
                         "sexF" = "Female",
                         "race1" = "White",
                         "race2" = "Black or African American",
                         "race3" = "Asian",
                         "race4" = "American Indian or Alaska Native",
                         "race5" = "Native Hawaiian or other Pacific Islander",
                         "race6" = "Multiple Responses Selected",
                         "race97" = "Other",
                         "hispanic0" = "Not Hispanic",
                         "hispanic1" = "Hispanic",
                         "education1" = "Less than High School",
                         "education2" = "High School Diploma",
                         "education3" = "Some College/Associates Degree",
                         "education4" = "Bachelor's Degree",
                         "education5" = "Graduate/Professional Degree",
                         "income1" = "< $10,000",
                         "income2" = "$10,000 to $14,999",
                         "income3" = "$15,000 to $24,999",
                         "income4" = "$25,000 to $34,999",
                         "income5" = "$35,000 to $49,999",
                         "income6" = "$50,000 to $74,999",
                         "income7" = "$75,000 to $99,999",
                         "income8" = "$100,000 or more",
                         "ncar0" = "0",
                         "ncar1" = "1",
                         "ncar2" = "2",
                         "ncar3+" = "3+",
                         "nhouse1" = "1 in House",
                         "nhouse2" = "2 in House",
                         "nhouse3" = "3 in House",
                         "nhouse4+" = "4+ in House"
)

D.fig$factor <- recode(D.fig$factor,
                       "sex" = "Sex",
                       "race" = "Race",
                       "hispanic" = "Hispanic",
                       "education" = "Education",
                       "income" = "Household Income",
                       "ncar" = "Cars",
                       "nhouse" = "Household Size")

D.fig <- D.fig %>%
  mutate(combined = as.factor(combined)) %>%
  mutate(Index = c(1:length(combined))) %>%
  mutate(Index = ifelse(combined == "$35,000 to $49,999", 23.5, ifelse(combined == "Some College/Associates Degree", 16.5, ifelse(combined == "2", 30.5, Index)))) %>%
  relocate(combined, .after = factor) %>%
  dplyr::filter(!(combined %in% c("sexNA", "raceNA", "hispanicNA", "educationNA", "incomeNA", "nhouseNA"))) %>%
  select(-group)

# reorder to remove 2 race categories and alphabetize sex, race, and hispanic status
D.fig <- D.fig %>% 
  dplyr::filter(combined != "American Indian or Alaska Native") %>% 
  dplyr::filter(combined != "Native Hawaiian or other Pacific Islander") %>%
  mutate(Index = ifelse(factor == "Race", ifelse(combined == "Asian", 3, 
                                                 ifelse(combined == "American Indian or Alaska Native", 4, 
                                                        ifelse(combined == "Black or African American", 5, 
                                                               ifelse(combined == "Multiple Responses Selected", 6, 
                                                                      ifelse(combined == "Native Hawaiian or other Pactific Islander", 7, 
                                                                             ifelse(combined == "Other", 8, 
                                                                                    ifelse(combined == "White", 9, Index))))))), Index)) %>%
  mutate(Index = ifelse(factor == "Hispanic", ifelse(combined == "Not Hispanic", 11, ifelse(combined == "Hispanic", 10, Index)), Index)) %>%
  mutate(Index = ifelse(factor == "Sex", ifelse(combined == "Male", 2, 1), Index))

saveRDS(D.fig, file = "./R/data/prevalence.fig.2022.rds")
```

Generate forest plot of 2022 prevalence by subgroups. 

```{r prevalence_forest, echo = TRUE, results = "show", message = TRUE}
p <- D.fig %>%
  ggplot(aes(y = reorder(combined, desc(Index)), x = prevalence, xmin = lower, xmax = upper)) +
  geom_point(aes(), color = "black", pch = 21) +
  geom_errorbarh(height = 0.1) +
  geom_vline(xintercept = D0$p, color = "black", linetype = "dashed", alpha = 0.5) +
  facet_grid(factor ~ ., scales = "free", space = "free", labeller = labeller(varname = label_wrap_gen(width = 10))) +
  ggtitle("\nPrevalence of Active Travel (2022)\n") +
  xlab("\nPrevalence\n") +
  ylab("") +
  theme(text = element_text(family = "Times", size = 12, color = "black")) + # overall text formatting
  theme(plot.title = element_text(hjust = 0.5)) + # center plot title
  theme(panel.spacing = unit(0.5, "lines"), strip.text.y = element_text(angle = 0, vjust = 0.5, hjust = 0.5, size = 12)) + # adjust facet panel spacing, text formatting
  coord_cartesian(clip = "off") + # prevent clipping of geom_points at facet panel margins
  theme_bw()

p %>% ggsave(filename = "./figure/USA_2022_forest_prevalence.png", width = 6.5, height = 9, units = "in")
```

#### Regression modeling

Run logistic regression for prevalence by all variables: sex, age, income, education, household cars, race*hispanic status, household size.

```{r allall, echo = TRUE, results = "show", message = TRUE}
glm.obj <- D %>% 
  mutate(I_TA = TA > 0) %>% 
  glm(I_TA ~ sex + age.numeric + income + education + ncar + race*hispanic + nhouse, family = "binomial", data = .)

summary(glm.obj)
anova(glm.obj, test = "Chisq")
saveRDS(glm.obj, file = "./R/data/glm.prevalence.2022.rds")

foo <- as.data.frame(summary(glm.obj)$coeff)
names(foo) <- c("estimate","sd","z","p")
foo <- foo %>% 
  select(estimate, sd, p) %>% 
  mutate( lower = estimate - 1.96*sd, upper = estimate + 1.96*sd) %>% 
  mutate( OR = exp(estimate), OR.lower = exp(lower), OR.upper = exp(upper)) %>% 
  select(estimate, OR, p, OR.lower, OR.upper)

write.csv(foo, file = "./R/data/prevalence.regression.2022.csv", quote = FALSE)
```

### Participation

#### Subgroup analysis

Next, participation by all variables: sex, age, income, education, household cars, race*hispanic status, household size.

```{r participation_D, echo = TRUE, results = "show", message = TRUE}
(D0 <- D %>% summarise(p = weighted.mean(pi, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se))

D1 <- D %>% group_by(sex) %>% summarise(p = weighted.mean(pi, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("sex")) %>% select(factor, group = sex, participation = p, lower, upper)

D2 <- D %>% group_by(race) %>% summarise(p = weighted.mean(pi, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("race")) %>% select(factor, group = race, participation = p, lower, upper)

D3 <- D %>% group_by(hispanic) %>% summarise(p = weighted.mean(pi, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("hispanic")) %>% select(factor, group = hispanic, participation = p, lower, upper)

D4 <- D %>% group_by(education) %>% summarise(p = weighted.mean(pi, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("education")) %>% select(factor, group = education, participation = p, lower, upper)

D5 <- D %>% group_by(income) %>% summarise(p = weighted.mean(pi, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("income")) %>% select(factor, group = income, participation = p, lower, upper)

D6 <- D %>% group_by(ncar) %>% summarise(p = weighted.mean(pi, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("ncar")) %>% select(factor, group = ncar, participation = p, lower, upper)

D7 <- D %>% group_by(nhouse) %>% summarise(p = weighted.mean(pi, weight.person/sum(weight.person)), se = sqrt(p*(1-p)*sum((weight.person/sum(weight.person))^2)), lower = p - 1.96*se, upper = p  + 1.96*se) %>% mutate(factor = factor("nhouse")) %>% select(factor, group = nhouse, participation = p, lower, upper)

D.all <- rbind(D1,D2,D3,D4,D5,D6,D7)

```


```{r participation_recode, echo = TRUE, results = "show", message = TRUE}
D.fig <- D.all %>% mutate(combined = paste0(factor, group)) %>% mutate(factor = factor(factor, levels = c("sex", "race", "hispanic", "income", "education", "ncar", "nhouse")))

D.fig$combined <- recode(D.fig$combined, 
                         "sexM" = "Male",
                         "sexF" = "Female",
                         "race1" = "White",
                         "race2" = "Black or African American",
                         "race3" = "Asian",
                         "race4" = "American Indian or Alaska Native",
                         "race5" = "Native Hawaiian or other Pacific Islander",
                         "race6" = "Multiple Responses Selected",
                         "race97" = "Other",
                         "hispanic0" = "Not Hispanic",
                         "hispanic1" = "Hispanic",
                         "education1" = "Less than High School",
                         "education2" = "High School Diploma",
                         "education3" = "Some College/Associates Degree",
                         "education4" = "Bachelor's Degree",
                         "education5" = "Graduate/Professional Degree",
                         "income1" = "< $10,000",
                         "income2" = "$10,000 to $14,999",
                         "income3" = "$15,000 to $24,999",
                         "income4" = "$25,000 to $34,999",
                         "income5" = "$35,000 to $49,999",
                         "income6" = "$50,000 to $74,999",
                         "income7" = "$75,000 to $99,999",
                         "income8" = "$100,000 or more",
                         "ncar0" = "0",
                         "ncar1" = "1",
                         "ncar2" = "2",
                         "ncar3+" = "3+",
                         "nhouse1" = "1 in House",
                         "nhouse2" = "2 in House",
                         "nhouse3" = "3 in House",
                         "nhouse4+" = "4+ in House"
)

D.fig$factor <- recode(D.fig$factor, 
                       "sex" = "Sex", 
                       "race" = "Race", 
                       "hispanic" = "Hispanic", 
                       "education" = "Education", 
                       "income" = "Household Income", 
                       "ncar" = "Cars", 
                       "nhouse" = "Household Size")

D.fig <- D.fig %>%
  mutate(combined = as.factor(combined)) %>%
  mutate(Index = c(1:length(combined))) %>%
  mutate(Index = ifelse(combined == "$35,000 to $49,999", 23.5, ifelse(combined == "Some College/Associates Degree", 16.5, ifelse(combined == "2", 30.5, Index)))) %>%
  relocate(combined, .after = factor) %>%
  dplyr::filter(!(combined %in% c("sexNA", "raceNA", "hispanicNA", "educationNA", "incomeNA", "nhouseNA"))) %>%
  select(-group)

# reorder to remove 2 race categories and alphabetize sex, race, and hispanic status
D.fig <- D.fig %>% 
  dplyr::filter(combined != "American Indian or Alaska Native") %>% 
  dplyr::filter(combined != "Native Hawaiian or other Pacific Islander") %>%
  mutate(Index = ifelse(factor == "Race", ifelse(combined == "Asian", 3, 
                                                 ifelse(combined == "American Indian or Alaska Native", 4, 
                                                        ifelse(combined == "Black or African American", 5, 
                                                               ifelse(combined == "Multiple Responses Selected", 6, 
                                                                      ifelse(combined == "Native Hawaiian or other Pactific Islander", 7, 
                                                                             ifelse(combined == "Other", 8, 
                                                                                    ifelse(combined == "White", 9, Index))))))), Index)) %>%
  mutate(Index = ifelse(factor == "Hispanic", ifelse(combined == "Not Hispanic", 11, ifelse(combined == "Hispanic", 10, Index)), Index)) %>%
  mutate(Index = ifelse(factor == "Sex", ifelse(combined == "Male", 2, 1), Index))

saveRDS(D.fig, file = "./R/data/participation.fig.2022.rds")
```

```{r participation_forest, echo = TRUE, results = "show", message = TRUE}
p <- D.fig %>% 
  ggplot(aes(y = reorder(combined, desc(Index)), x = participation, xmin = lower, xmax = upper)) + 
  geom_point(aes(), color = "black", pch = 21) +   
  geom_errorbarh(height = 0.1) +   
  geom_vline(xintercept = D0$p, color = "black", linetype = "dashed", alpha = 0.5) + 
  facet_grid(factor ~ ., scales = "free", space = "free", labeller = labeller(varname = label_wrap_gen(width = 10))) + 
  ggtitle("\nParticipation in Active Travel (2022)\n") +
  xlab("\nParticipation\n") +
  ylab("") +
  theme(text = element_text(family = "Times", size = 12, color = "black")) + # overall text formatting
  theme(plot.title = element_text(hjust = 0.5)) + # center plot title
  theme(panel.spacing = unit(0.5, "lines"), strip.text.y = element_text(angle = 0, vjust = 0.5, hjust = 0.5, size = 12)) + # adjust facet panel spacing, text formatting
  coord_cartesian(clip = "off") + # prevent clipping of geom_points at facet panel margins
  theme_bw()

p %>% ggsave(filename = "./figure/USA_2022_forest_participation.png", width = 6.5, height = 9, units = "in")
```

#### Regression modeling

```{r part_glm, echo = TRUE, results = "show", message = TRUE}
glm.obj <- D %>% glm(pi ~ sex + age.numeric + income + education + ncar + race*hispanic + nhouse, family = "binomial", data = .)
summary(glm.obj)
anova(glm.obj, test = "Chisq")

saveRDS(glm.obj, file = "./R/data/glm.participation.2022.rds")

foo <- as.data.frame(summary(glm.obj)$coeff)
names(foo) <- c("estimate","sd","z","p")
foo <- foo %>% select(estimate, sd, p) %>% mutate( lower = estimate - 1.96*sd, upper = estimate + 1.96*sd) %>% mutate( OR = exp(estimate), OR.lower = exp(lower), OR.upper = exp(upper)) %>% select(estimate, OR, p, OR.lower, OR.upper)
write.csv(foo, file = "./R/data/participation.regression.2022.csv", quote = FALSE)
```



# R Session Info

```{r sessionInfo, results = "show", warning = FALSE, error = FALSE}
date()
sysInfo <- Sys.info()
sysInfo[names(sysInfo) != "version"]
sessionInfo()
```


